# SPDX-FileCopyrightText: 2021-2024 SanderTheDragon <sanderthedragon@zoho.com>
#
# SPDX-License-Identifier: CC0-1.0

build:
  stage: build
  image: python:3.12-bookworm
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - pip install -r "$CI_PROJECT_DIR/requirements.txt"
  script:
    - hatch build
  artifacts:
    paths:
      - "dist/"

publish:
  stage: deploy
  image: python:3.12-bookworm
  rules:
    - if: $CI_COMMIT_TAG
  dependencies:
    - build
  id_tokens:
    PYPI_ID_TOKEN:
      aud: pypi
  script:
    - apt update && apt install -y jq
    - python -m pip install -U twine id

    - oidc_token=$(python -m id PYPI)
    - resp=$(curl -X POST "https://pypi.org/_/oidc/mint-token" -d "{\"token\":\"${oidc_token}\"}")
    - api_token=$(jq --raw-output '.token' <<< "${resp}")

    - twine upload -u __token__ -p "${api_token}" "dist/*"

pages:
  stage: deploy
  image: python:3.12-bookworm
  rules:
    - if: $CI_COMMIT_TAG
  before_script:
    - pip install -r "$CI_PROJECT_DIR/requirements.txt"
  script:
    - sphinx-build -b html docs public
  artifacts:
    paths:
      - "public/"

release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  dependencies:
    - publish
  script:
    echo "Release $CI_COMMIT_TAG"
  release:
    tag_name: "$CI_COMMIT_TAG"
    description: "$CI_COMMIT_TAG"
    assets:
      links:
        - name: "PyPI"
          url: "https://pypi.org/project/sanderthedragon-sphinxext/$CI_COMMIT_TAG/#files"
